'use strict';

module.exports.cadastro = function(application, req, res){
	res.render('cadastro', {validacao: {}, dadosForm: {}});
}

module.exports.cadastrar = function(application, req, res){
	let dadosForm = req.body;

	req.assert('nome', 'Nome não pode ser vazio').notEmpty();
	req.assert('usuario', 'Usuário não pode ser vazio').notEmpty();
	req.assert('senha', 'Senha não pode ser vazio').notEmpty();
	req.assert('casa', 'Casa não pode ser vazio').notEmpty();

	let erros  = req.validationErrors();

	if(erros){
		res.render('cadastro', {validacao: erros, dadosForm: dadosForm});
		return;
	}

	let connection = application.config.dbConnection;

	let usuariosDAO = new application.app.models.UsuarioDAO(connection);
	let jogoDAO = new application.app.models.JogoDAO(connection);

	usuariosDAO.inserir(dadosForm);

	jogoDAO.gerarParametros(dadosForm.usuario);

	res.send('podemos cadastrar');
}