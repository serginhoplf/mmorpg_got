var mongo = require('mongodb');

var connMongo = function(){

	var db = new mongo.Db(
		'got', //Nome
		new mongo.Server(
			'localhost', 	// endereço do servidor do banco
			27017,			// porta de conexão
			{}
		),
		{}
	);

	return db;
}

module.exports = function(){
	return connMongo;
}